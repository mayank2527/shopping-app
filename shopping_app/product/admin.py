from django.contrib import admin
from .models import Cart,Order,OrderDetails,Product,User


admin.site.register(User)
admin.site.register(Cart)
admin.site.register(Order)
admin.site.register(Product)