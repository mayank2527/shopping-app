from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    mobile=models.BigIntegerField(null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    updated_at:models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'user'

class ModelBase(models.Model):
    created_at = models.DateTimeField(auto_now_add=True,null=True)
    updated_at = models.DateTimeField(auto_now=True,null=True)

    class Meta:
        abstract = True
        managed = False

class Product(ModelBase):
    name = models.CharField(max_length=200)
    mrp=models.IntegerField()
    stock=models.IntegerField(default=0)
    image=models.ImageField(upload_to='images',default=None)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'product'

class Cart(ModelBase):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    product=models.ForeignKey(Product,on_delete=models.CASCADE)
    quantity=models.IntegerField()

    def __str__(self):
        return self.user.username+" cart"

    class Meta:
        db_table = 'cart'

class Order(models.Model):
    user=models.ForeignKey(User,on_delete=models.SET_NULL,null=True)
    status=models.CharField(max_length=20)
    created_at=models.DateTimeField(auto_now_add=True,null=True)
    updated_at = models.DateTimeField(auto_now=True,null=True)
    arrived_at=models.DateTimeField(default=None)
    amount=models.IntegerField()
    ref_code=models.CharField(max_length=20)

    def __str__(self):
        return self.user.username+" order"

    class Meta:
        db_table = 'order'

class OrderDetails(ModelBase):
    order=models.ForeignKey(Order,on_delete=models.SET_NULL,null=True)
    product=models.ForeignKey(Product,on_delete=models.SET_NULL,null=True)
    quantity=models.IntegerField()

    class Meta:
        db_table = 'order_details'

class Transaction(models.Model):
    token = models.CharField(max_length=120)
    order=models.ForeignKey(Order,on_delete=models.SET_NULL,null=True)
    success = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.order

    class Meta:
        db_table = 'transaction'
        ordering = ['-timestamp']