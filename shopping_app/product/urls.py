from django.urls import path
from django.conf.urls import url
from . import views,models
from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView

app_name = "product"
urlpatterns = [
    url(r'home', views.IndexView.as_view(),name='home'),
    path('shop/', views.ShopView.as_view(), name='shop'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('cart/', views.CartListView.as_view(), name='cart'),
    path('add/', views.add_to_cart, name='add'),
    path('<int:cart_id>/update', views.update_cart, name='update_cart'),
    path('<int:cart_id>/delete', views.delete_cart, name='remove_cart_product'),
    path('checkout/', TemplateView.as_view(template_name='product/checkout.html'), name='checkout'),
    path('payment/', views.payment, name='payment'),
    path('order/', login_required(views.OrderListView.as_view()), name='order'),
    path('<int:pk>/order', login_required(views.OrderDetailView.as_view()), name='order_detail'),
    path('<int:pk>/invoice', login_required(views.GeneratePdf.as_view()), name='invoice'),
]