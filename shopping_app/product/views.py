from django.shortcuts import get_object_or_404, render,redirect
from django.conf import settings
from django.contrib import messages
from django.views import generic
from django.urls import reverse
from .models import Product,Cart,User,Order,OrderDetails,Transaction
from django.http import HttpResponseRedirect,HttpResponse
from .extras import generate_order_id
import stripe
import json
from django.db.models import Sum
from django.db import transaction
from django.contrib.auth.decorators import login_required
import logging
from django.core.mail import send_mail
from .utils import render_to_pdf
from datetime import datetime
logger = logging.getLogger(__name__)
stripe.api_key = settings.STRIPE_SECRET_KEY
# Create your views here.

class IndexView(generic.ListView):
    model = Product
    template_name = 'product/home.html'
    context_object_name = 'product_list'

class ShopView(generic.ListView):
    model = Product
    template_name = 'product/shop.html'
    context_object_name = 'product_list'

class DetailView(generic.DetailView):
    model = Product
    template_name = 'product/detail.html'
    context_object_name = 'product_details'
    
    def get_template_names(self):
        if self.request.is_ajax():
            template_name = 'product/partial-details.html'
        else:
            template_name = 'product/detail.html'
        return [template_name]
    # def get_queryset(self):
    #     if self.request.user.id!=None:
    #         user = self.request.user.id
    #         print(user)
    #         print(self.kwargs)
    #         return Product.objects.filter(id=self.kwargs["pk"])
            
            
    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        if self.request.user.id!=None:
            cart=Cart.objects.prefetch_related('user').filter(user_id=self.request.user.id)
            for item in cart:
                if item.product.id==self.kwargs["pk"]:
                    context['cart']=True
                    return context
            context['cart']=False
        return context

class CartListView(generic.ListView):
    model = Cart
    template_name = 'product/cart.html'
    context_object_name = 'cart_list'

    def get_queryset(self):
        if self.request.user.id!=None:
            return Cart.objects.filter(user=self.request.user.id)
        else:
            if 'cart' in self.request.GET :
                data=json.loads(self.request.GET['cart'])
                self.request.session['cart']=data
                return data
            else:
                return []

    def get_template_names(self):
        if self.request.is_ajax():
            template_name = 'product/partial-cart.html'
        else:
            template_name = 'product/cart.html'
        return [template_name]


def add_to_cart(request):
    if request.user.id!=None:
        if request.method == 'POST':
            cart=Cart.objects.create(product=Product.objects.get(pk=request.POST['product']),user=User.objects.get(pk=request.user.id),quantity=1)
            cart.save()
            return HttpResponseRedirect(reverse('product:detail', args=(request.POST['product'],)))
            # return render(request, 'product/partial-details.html')
    else:
        return HttpResponseRedirect(reverse('product:detail', args=(request.POST['product'],)))

def update_cart(request, cart_id):
    if request.user.id!=None:
        cart = get_object_or_404(Cart, pk=cart_id)
        cart.quantity = request.POST['quantity']
        cart.save()
        return HttpResponseRedirect(reverse('product:cart'))

def delete_cart(request, cart_id):
    cart = get_object_or_404(Cart, pk=cart_id)
    cart.delete()
    return HttpResponseRedirect(reverse('product:cart'))

@login_required
def payment(request):
    if('cart' in request.session and request.session['cart']!=None):
        for item in request.session['cart']:
            Cart.objects.create(user=User.objects.get(pk=request.user.id),product=Product.objects.get(pk=item['id']),quantity=item['quantity'])
        request.session['cart']=None
        return HttpResponseRedirect(reverse('product:cart'))

    cart=Cart.objects.filter(user=request.user.id)
    amount=0
    for item in cart:
        amount+=item.product.mrp*item.quantity
    publishKey = settings.STRIPE_PUBLISHABLE_KEY
    if request.method == 'POST':
        token = request.POST.get('stripeToken', False)
        try:
            charge = stripe.Charge.create(
                amount=amount*100,
                currency='usd',
                description='Example charge',
                source=token,
            )
            return checkout(request,token=token)
        except stripe.error.CardError as e:
            messages.info(request, "Your card has been declined.")
    return HttpResponseRedirect(reverse('product:cart'))


@login_required
@transaction.atomic
def checkout(request,token):
    cart=Cart.objects.filter(user=request.user.id)
    if len(cart)!=0:
        amount=0
        for item in cart:
            amount+=item.product.mrp*item.quantity
        order=Order.objects.create(user=User.objects.get(pk=request.user.id),amount=amount,ref_code=generate_order_id(),status="processing")
        order.save()
        order_details=[]
        for item in cart:
            order_details.append(OrderDetails(order=order,product=item.product,quantity=item.quantity))
        cart.delete()
        OrderDetails.objects.bulk_create(order_details)
        trans=Transaction.objects.create(token=token,order=order)
        if trans:
            subject = 'Order Confirmation'
            message = ' Thank you for order '
            email_from = settings.EMAIL_HOST_USER
            recipient_list = ['mayankchhabra2527@gmail.com',]
            send_mail( subject, message, email_from, recipient_list )
        return HttpResponseRedirect(reverse('product:order'))
    else:
        return HttpResponseRedirect(reverse('product:home'))

class OrderListView(generic.ListView):
    model = Order
    template_name = 'product/order.html'
    context_object_name = 'order_list'

class OrderDetailView(generic.DetailView):
    model = Order
    template_name = 'product/order_detail.html'
    context_object_name = 'order'

class GeneratePdf(generic.View):
    context_object_name = 'order'

    def get(self, request, *args, **kwargs):
        data=Order.objects.get(pk=self.kwargs["pk"])
        pdf = render_to_pdf('product/invoice.html', {'order':data})
        return HttpResponse(pdf, content_type='application/pdf')