"""shopping_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings 
from django.conf.urls.static import static 
from django.conf.urls import url, include
from django.views.generic import TemplateView
from allauth.account import views
from product import views as product_view

urlpatterns = [
    path('', product_view.IndexView.as_view(),name='home'),
    url(r'^accounts/login/', views.login, name="account_login"),
    url(r'^accounts/signup/', views.signup, name="account_signup"),
    url(r'^accounts/', include('allauth.urls')),
    path('mall/', include("product.urls")),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)

